import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

export const barComponentOptions = {
    selector: 'chart-bar',
    template: `<div>
                <div style="display: block; width: 500px; height: 300px;">
                    <canvas height="auto" width="auto" 
                        baseChart class="chart" 
                        [datasets]="horizontalBarChartData" 
                        [labels]="horizontalBarChartLabels"
                        [options]="horizontalBarChartOptions" 
                        [chartType]="horizontalBarChartType" 
                        [colors]="horizontalBarChartColors" 
                        (chartClick)="onClickChart($event)">
                    </canvas>
                </div>
                </div>`,
    style: [``]
};

@Component(barComponentOptions)

export class ChartBarComponent implements OnInit {

    @Input() horizontalBarChartData: any = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
    ];
    @Input() horizontalBarChartLabels: any = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    @Input() horizontalBarChartColors: any;
    @Input() horizontalBarChartTooltips: any;
    @Output() sendData: EventEmitter<any> = new EventEmitter();

    public horizontalBarChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: false,
        legend: { display: false, position: 'bottom' },
        scales: {
            xAxes: [{
                stacked: false,
                gridLines: { display: false },
                ticks: {
                    beginAtZero: true,
                    callback: function (label: any, index: any, labels: any) {
                        if (Math.floor(label) === label) {
                            return (label >= 1000) ? label / 1000 + 'k' : label;
                        }
                    }
                }
            }],
            yAxes: [{
                stacked: false
            }],
        }, // scales
        tooltips: {
            mode: 'label',
            callbacks: {
                title: function (tooltipItem: any, data: any) {
                    return tooltipItem[0].yLabel;
                },
                label: function (tooltipItem: any, data: any) {
                    let tooltip_label = tooltipItem.xLabel;
                    if (typeof data.datasets[0].label !== 'undefined') {
                        tooltip_label += data.datasets[0].label;
                    }
                    return tooltip_label;
                }
            }
        },
    };
    public horizontalBarChartType: String = 'horizontalBar';
    constructor() { }
    ngOnInit() {
        if (typeof this.horizontalBarChartTooltips !== 'undefined') {
            this.horizontalBarChartOptions.tooltips = this.horizontalBarChartTooltips;
        }
    }

    onClickChart(event: any) {
        this.sendData.emit(event);
    }
}