import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

const short_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export const columnComponentoptions = {
    selector: 'chart-column',
    template: `<div>
                <div style="display: block; width: 500px; height: 300px;">
                    <canvas height="auto" width="auto"
                        baseChart class="chart"
                        [datasets]="columnChartData"
                        [labels]="columnChartLabels"
                        [options]="barChartOptions"
                        [legend]="barChartLegend"
                        [chartType]="barChartType"
                        [colors]="columnChartColors"
                        (chartClick)="onClickChart($event)"></canvas>
                </div>
                </div>`,
    style: [``]
};

@Component(columnComponentoptions)

export class ChartColumnComponent implements OnInit {

    @Input() columnChartData: any = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
    ];
    @Input() columnChartLabels: any = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    @Input() columnChartLegend: Boolean = true;
    @Input() columnChartTooltips: any;
    @Input() columnChartColors: any;
    @Input() columnChartStacked: any;
    @Output() columnChartClickData: EventEmitter<any> = new EventEmitter();
    @Output() sendData: EventEmitter<any> = new EventEmitter();

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: false,
        legend: { display: true, position: 'bottom' },
        scales: {
            xAxes: [{
                stacked: true,
                gridLines: { display: false },
                ticks: {
                    autoSkip: true,
                    maxRotation: 0
                }
            }],
            yAxes: [{
                stacked: false,
                ticks: {
                    beginAtZero: true,
                    callback: function (label: any, index: any, labels: any) {
                        if (Math.floor(label) === label) {
                            return (label >= 1000) ? label / 1000 + 'k' : label;
                        }
                    }
                }
            }],
        }, // scales
        tooltips: {
            mode: 'label',
            callbacks: {
                label: function (tooltipItem: any, data: any) {
                    return data.datasets[tooltipItem.datasetIndex].label + ': ' + tooltipItem.yLabel;
                }
            }
        },
    };

    public barChartType: string = 'bar';

    constructor() { }

    ngOnInit() {
        if (typeof this.columnChartLabels === 'undefined') {
            this.columnChartLabels = short_months;
        }
        if (typeof this.columnChartLegend !== 'undefined') {
            this.barChartOptions.legend.display = this.columnChartLegend;
        }
        if (typeof this.columnChartTooltips !== 'undefined') {
            this.barChartOptions.tooltips.callbacks = this.columnChartTooltips;
        }
        if (typeof this.columnChartStacked !== 'undefined') {
            this.barChartOptions.scales.xAxes[0].stacked = this.columnChartStacked;
        }
    }

    onClickChart(event: any) {
        if (typeof event.active[0] !== 'undefined' && typeof event.active[0]._index !== 'undefined') {
            this.columnChartClickData.emit(event);
        }
    }

}